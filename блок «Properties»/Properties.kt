// 1. Add a custom setter to PropertyExample.propertyWithCounter so that the counter property is incremented every time the propertyWithCounter is assigned.

class PropertyExample() {
    var counter = 0
    var propertyWithCounter: Int? = null
    set(value) {
        field = value
        counter++;
    }
}
// 2. Add a custom getter to make the val lazy really lazy. It should be initialized by invoking initializer() during the first access.
class LazyProperty(val initializer: () -> Int) {
    val lazy: Int
        get() {
            if (support == null)
            	support = initializer();
            return support!!;
        }
        
    var support : Int? = null;
}
// 3. Learn about delegated properties and make the property lazy using delegates.
class LazyProperty(val initializer: () -> Int) {
    val lazyValue: Int by lazy {initializer()}
}
// 4. You can declare your own delegates. Implement the methods of the class EffectiveDate so you can delegate to it. Store only the time in milliseconds in the timeInMillis property.
//Use the extension functions MyDate.toMillis() and Long.toDate(), defined in MyDate.kt.


import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class D {
    var date: MyDate by EffectiveDate()
}

class EffectiveDate<R> : ReadWriteProperty<R, MyDate> {

    var timeInMillis: Long? = null

    override fun getValue(thisRef: R, property: KProperty<*>): MyDate {
        return timeInMillis!!.toDate();
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: MyDate) {
        timeInMillis = value.toMillis();
    }
}
