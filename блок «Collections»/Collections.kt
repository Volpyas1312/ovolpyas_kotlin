// 1. Implement the extension function Shop.getSetOfCustomers(). The class Shop and all related classes can be found in Shop.kt.
fun Shop.getSetOfCustomers(): Set<Customer> = customers.toSet()
// 2. Return a list of customers, sorted in the descending by number of orders they have made
fun Shop.getCustomersSortedByOrders(): List<Customer> = customers.sortedByDescending{ it.orders.count() }
// 3.
// Find all the different cities the customers are from
fun Shop.getCustomerCities(): Set<City> = customers.map{it.city}.toSet()

// Find the customers living in a given city
fun Shop.getCustomersFrom(city: City): List<Customer> = customers.filter{ it.city == city}
// 4.
// Return true if all customers are from a given city
fun Shop.checkAllCustomersAreFrom(city: City): Boolean = customers.all{it.city == city}

// Return true if there is at least one customer from a given city
fun Shop.hasCustomerFrom(city: City): Boolean = customers.any{it.city == city}

// Return the number of customers from a given city
fun Shop.countCustomersFrom(city: City): Int = customers.count{it.city == city}

// Return a customer who lives in a given city, or null if there is none
fun Shop.findCustomerFrom(city: City): Customer? = customers.find{it.city == city}
// 5.
// Build a map from the customer name to the customer
fun Shop.nameToCustomerMap(): Map<String, Customer> = customers.associateBy{it.name}

// Build a map from the customer to their city
fun Shop.customerToCityMap(): Map<Customer, City> = customers.associateWith{it.city}

// Build a map from the customer name to their city
fun Shop.customerNameToCityMap(): Map<String, City> = customers.associate{it.name to it.city}
// 6.
// Build a map that stores the customers living in a given city
fun Shop.groupCustomersByCity(): Map<City, List<Customer>> = customers.groupBy{it.city}
// 7.
// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> = customers.filter{
    var (positive, negative) = it.orders.partition{it.isDelivered};
    negative.count() > positive.count();
}.toSet();
// 8.
// Return all products the given customer has ordered
fun Customer.getOrderedProducts(): List<Product> = this.orders.flatMap{it.products}

// Return all products that were ordered by at least one customer
fun Shop.getOrderedProducts(): Set<Product> = customers.flatMap{it.orders.flatMap{it.products}}.toSet()
// 9.
// Return a customer who has placed the maximum amount of orders
fun Shop.getCustomerWithMaxOrders(): Customer? = customers.maxByOrNull{it.orders.count()}

// Return the most expensive product that has been ordered by the given customer
fun getMostExpensiveProductBy(customer: Customer): Product? 
    = customer.orders.flatMap{it.products}.maxByOrNull{it.price};
// 10.
// Return the sum of prices for all the products ordered by a given customer
fun moneySpentBy(customer: Customer): Double = customer.orders.flatMap{it.products}.sumOf{it.price}
// 11.
// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> =
    customers.map{it.getOrderedProducts()}.reduce{sum, element -> sum.intersect(element)}

fun Customer.getOrderedProducts(): Set<Product> = this.orders.flatMap{it.products}.toSet()
// 12.
// Find the most expensive product among all the delivered products
// ordered by the customer. Use `Order.isDelivered` flag.
fun findMostExpensiveProductBy(customer: Customer): Product?
    = customer.orders.filter{it.isDelivered}.flatMap{it.products}.maxByOrNull{it.price}

// Count the amount of times a product was ordered.
// Note that a customer may order the same product several times.
fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int
    = customers.flatMap{it.getOrderedProducts()}.filter{it == product}.count()

fun Customer.getOrderedProducts(): List<Product> = this.orders.flatMap{it.products}
// 13.
// Find the most expensive product among all the delivered products
// ordered by the customer. Use `Order.isDelivered` flag.
fun findMostExpensiveProductBy(customer: Customer): Product? 
= customer.orders.asSequence().filter{it.isDelivered}.flatMap{it.products}.maxByOrNull{it.price}

// Count the amount of times a product was ordered.
// Note that a customer may order the same product several times.
fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int
= customers.asSequence().flatMap{it.getOrderedProducts()}.filter{it == product}.count()

fun Customer.getOrderedProducts(): Sequence<Product> = this.orders.asSequence().flatMap{it.products}
// 14. We can rewrite and simplify the following code using lambdas and operations on collections. Fill in the gaps in doSomethingWithCollection, the simplified version of the doSomethingWithCollectionOldStyle function, so that its behavior stays the same and isn't modified in any way.
fun doSomethingWithCollection(collection: Collection<String>): Collection<String>? {

    val groupsByLength = collection.groupBy { s -> s.count() }

    val maximumSizeOfGroup = groupsByLength.values.map { group -> group.size }.maxOrNull()

    return groupsByLength.values.firstOrNull { group -> group.size == maximumSizeOfGroup }
}
