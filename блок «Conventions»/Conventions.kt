// 1. Learn about operator overloading and how the different conventions for operations like ==, <, + work in Kotlin. Add the function compareTo to the class MyDate to make it comparable. After this, the code below date1 < date2 should start to compile.
import java.util.Date
data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    override fun compareTo(other : MyDate) : Int
    {
        val first : Date = Date(year, month, dayOfMonth);
        val second : Date = Date(other.year, other.month, other.dayOfMonth);
        if(first == second) return 0;
        else if (first > second) return 1;
        else return -1;
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
// 2. Using ranges implement a function that checks whether the date is in the range between the first date and the last date (inclusive).
fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}
// 3. Make the class DateRange implement Iterable<MyDate>, so that it can be iterated over. Use the function MyDate.followingDate() defined in DateUtil.kt; you don't have to implement the logic for finding the following date on your own.
class DateRange : Iterable<MyDate>
    {
        var now : MyDate;
        val end : MyDate;
        constructor(start: MyDate, end: MyDate) 
        {
            now = start;
            this.end = end;
        }
        override fun iterator() : Iterator<MyDate>  = object : Iterator<MyDate>{
            override fun next() : MyDate 
            { 
                val prev = now
                now = now.followingDate();
                return prev;
            }
        	override fun hasNext() : Boolean = now <= end;
        }
    }

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
// 4.First, add the extension function plus() to MyDate, taking the TimeInterval as an argument. Use the utility function MyDate.addTimeIntervals() declared in DateUtil.kt
// Then, try to support adding several time intervals to a date. You may need an extra class.

import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

// Supported intervals that might be added to dates:
enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate = this.addTimeIntervals(timeInterval, 1);
operator fun MyDate.plus(timeInterval: TimeIntervalAmount): 
	MyDate = this.addTimeIntervals(timeInterval.timeInterval, timeInterval.amount);

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}

operator fun TimeInterval.times(count : Int) : TimeIntervalAmount = TimeIntervalAmount(this, count);

class TimeIntervalAmount(val timeInterval : TimeInterval, val amount : Int)
// 5. Implement the function Invokable.invoke() to count the number of times it is invoked.
class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
        numberOfInvocations++;
        return this;
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
