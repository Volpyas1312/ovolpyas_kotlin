fun main() {
    val backgroundThread = Thread {
        Thread.sleep(1000L)
        println("World")
    }

    backgroundThread.start()
    Thread.sleep(2000L)
    println("Hello,")
    backgroundThread.join()
}
